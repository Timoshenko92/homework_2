package com.company;

import java.util.Random;

/**
 * Created by org2 on 04.10.2016.
 */
public class SumDiagonal {
    public static void main(String[] args)
    {
        int [][] arr=new int[10][10];
        inputMas(arr);
        showMas(arr);
        System.out.println(sumDiagonal(arr));
    }
    public static void inputMas(int arr[][]) {

        Random rnd = new Random();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                arr[i][j] = (int) (Math.random() * 20);
            }
        }
    }
    public static void showMas(int arr[][]) {

        Random rnd = new Random();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.println(arr[i][j]+" ");
            }
        }
    }
    public static int sumDiagonal(int arr[][])
    {int sum=0;
        for (int i = 0; i <arr.length ; i++) {
            for (int j = 0; j <arr.length ; j++) {
                if( j<i)
                {
                    sum+=arr[i][j];
                }
            }

        }
        return sum;
    }
}
